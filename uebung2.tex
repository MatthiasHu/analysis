\documentclass{uebung}

\title{\bfseries 2.~Übung zur Analysis I} 
\author{Prof.\ Dr.\ Marc\ Nieper-Wißkirchen \and Caren\ Schinko, M.\ Sc.}  
\date{24.~Oktober 2016\thanks{Die bearbeiteten Übungsblatter sind bis
    9:55 Uhr am 31.~Oktober 2016 in den Analysis-Briefkasten einzuwerfen.}}

\begin{document}

\maketitle

\begin{enumerate}[start=7]

\item \writtenex
  Sind $A$ und $B$ zwei Aussagen, so ist
  \[
    A \implies B \quad\text{äquivalent zu}\quad \lnot B \implies \lnot A.
  \]

  (Tip: Stelle für beide Aussagen die Wahrheitstafeln auf; vergleiche
  dazu den Beweis des Theorems in Abschnitt 0.3 der Vorlesung.)

\item \oralex Sei $m \in \set Z$. Sei $A_m$, $A_{m + 1}$, $A_{m + 2}$,
  \dots\ eine Folge von Aussagen. Zeige folgende Modifikationen der
  vollständigen Induktion:
  \begin{enumerate}
  \item
    \begin{description}[wide=0pt]
    \item[Voraussetzung:]
      Es gelte
      \begin{enumerate}
      \item $A_m$ ist wahr,
      \item Ist $n \in \set Z$, $n \ge m$ und ist $A_n$ wahr, so ist
        auch $A_{n + 1}$ wahr.
      \end{enumerate}
    \item[Behauptung:]
      $A_n$ ist wahr für $n \in \set Z$ mit $n \ge m$.
    \end{description}
  \item
    \begin{description}[wide=0pt]
    \item[Voraussetzung:] Es gelte: Ist $n \in \set Z$, $n \ge m$ und
      gilt $A_k$ für $m \leq k < n$, so gilt $A_n$.
    \item[Behauptung:]
      $A_n$ ist wahr für $n \in \set Z$ mit $n \ge m$.
    \end{description}
  \end{enumerate}

\item \writtenex 
  \textbf{Berechnung der Bogenlänge des Einheitskreises
    über einer Sehne.}  Es sei $s$ eine Sehne der Länge $\ell$ des
  Einheitskreises.  Wir wollen die Länge des Bogens $B$ über der Sehne
  $s$ nach einem von Archimedes von Syrakus vorgeschlagenem Verfahren
  bestimmen.  Dabei wird $B$ durch Sehnenzüge $\Sigma_n$ (mit $n = 1$,
  $2$, $4$, $8$, $16$, \dots) gleichlanger Sehnen angenähert.  Die
  Länge einer Sehne von $\Sigma_n$ werde mit $b_n$ bezeichnet. Dabei
  ergibt sich $b_{2n}$ aus $b_n$ gemäß der folgenden Abbildung:
  \begin{center}
    \begin{tikzpicture}
      \coordinate (P) at (170: 4cm);
      \coordinate (Q) at (10: 4cm);
      \coordinate (R) at (90: 4cm);
      \coordinate (PR) at (130: 4cm);
      \coordinate (RQ) at (50: 4cm);
      \coordinate (PPR) at (150: 4cm);

      \node [fill=black, inner sep=1pt] at (0, 0) {};

      \draw (0, 0) circle [radius=4cm];
      \draw [name path=b1] (P) -- node [above, pos=0.67] {$b_1 = \ell$} (Q);
      \draw [name path=b2] (P) -- node [above, sloped] {$b_2$} (R) -- (Q);
      \draw (P) -- node [above, sloped, fill=white] {$b_4$} (PR) -- (R) -- (RQ) -- (Q);
      
      \draw [dashed] (0, 0) -- 
      node [right] {$h_1$} (intersection of (0, 0)--R and P--Q) {}; 
      \draw [dashed] (0, 0) -- 
      node [above, sloped, pos=0.67] {$h_2$} (intersection of (0, 0)--PR and P--R) {}; 
      \draw [dashed] (0, 0) -- 
      node [above, sloped, pos=0.67] {$h_4$} (intersection of (0, 0)--PPR and P--PR) {}; 
    \end{tikzpicture}
  \end{center} 
  Eine Annäherung an die Länge des Bogens $B$
  liefert dann die Folge $(B_{2^k})_{k \in \set N_0}$ mit
  $B_{2^k} = 2^k \cdot b_{2^k}$.
  \begin{enumerate}
  \item Entwickle ein Verfahren zur rekursiven Berechnung von
    $b_{2^k}$ und damit von $B_{2^k}$.
  \item Teste Dein Verfahren mit einem (Taschen-)Rechner mit dem
    Eingabewert $\ell = 2$.
  \item Schreibe für das Verfahren ein (Scheme-)Programm.
  \end{enumerate}

\item \writtenex
  \textbf{Binomialkoeffizienten.}
  Für $a \in \set R$ definieren wir die Folge der \emph{Binomial\-koeffizienten}
  $\binom a n$ für $n \in \set N_0$ rekursiv durch
  \[
  \binom a 0 \coloneqq 1 \quad \text{und} \quad
  \binom a {n + 1} \coloneqq \frac{a - n}{n + 1} \cdot \binom a n.
  \]
  Seien im folgenden $a \in \set R$ und $m$, $n \in \set N_0$. Zeige:
  \begin{enumerate}
  \item
    \label{it:binomial}
    $\displaystyle \binom a n + \binom a {n + 1} = \binom {a + 1}{n +
      1}$.
    \paragraph{Bemerkung.} In dieser Gleichung ist für die speziellen
    Werte $a \in \set N_0$ das Bildungsgesetz des \emph{Pascalschen
      Dreiecks} enthalten. Dieses ist eine Funktionstafel für die
    Werte der Binomialkoeffizienten $\binom a n$ für $a$,
    $n \in \set N_0$ mit $n \leq a$:
    \begin{center}
      \begin{tikzpicture}
        \matrix (M) [matrix of math nodes, row sep=0.25cm, column sep=0.25cm] 
        {
          & & &   &   &   &|(A)|& n & |(C)| \\
          & & &   &   & |(S1)| & a & |(T1)|  & 0 & |(X1)| \\
          & & &   & |(S2)| & 0 &   & 1 & |(T2)|  & 1 & |(X2)| \\
          & & & |(S3)| & 1 & & 1 &   & 1 & |(T3)| & 2 & |(X3)| \\
          & & |(S4)| & 2 &  & 1 &   & 2 &   & 1 & |(T4)| & 3 & |(X4)| \\
          & |(S5)| & 3 & & 1 & & 3 & & 1 & & 3 & |(T5)| & 4 & |(X5)| \\
          |(S6)| & 4 & |(D)| & 1 & |(Y1)| & 4 & |(Y2)| & 6 & |(Y3)| & 4 & |(Y4)| & 1 & |(B)|& \\
        };

        \draw (A.north west)--(B.south east);
        \draw (C.north east)--(D.south west);
        \draw (X1.north east)--(Y1.south west);
        \draw (X2.north east)--(Y2.south west);
        \draw (X3.north east)--(Y3.south west);
        \draw (X4.north east)--(Y4.south west);
        \draw (X5.north east)--(B.south west);

        \draw (S1.south west)--(T1.south west);
        \draw (S2.south west)--(T2.south west);
        \draw (S3.south west)--(T3.south west);
        \draw (S4.south west)--(T4.south west);
        \draw (S5.south west)--(T5.south west);
        \draw (S6.south west)--(B.south west);
      \end{tikzpicture}
    \end{center}
  \item
    Für $n \leq m$ gilt
    $\displaystyle \binom m n = \frac{m!}{n! (m - n)!} = \binom m {m - n}$.
  \item
    Für $n > m$ gilt $\displaystyle \binom m n = 0$.
  \item
    $\displaystyle \binom m n \in \set N_0$. 

    (Tip: Benutze \ref{it:binomial}.)
  \end{enumerate}

\end{enumerate}

\end{document}
